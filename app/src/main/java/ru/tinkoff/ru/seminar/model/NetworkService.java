package ru.tinkoff.ru.seminar.model;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkService {
    private static final Gson gson = new GsonBuilder()
            .registerTypeAdapter(Weather.class, new WeatherDeserializer())
            .registerTypeAdapter(WeatherList.class, new WeatherListDeserializer())
            .create();

    private static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://api.openweathermap.org/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build();

    private static final WeatherApi service = retrofit.create(WeatherApi.class);

    public static WeatherApi getApi(){
        return service;
    }
}
