package ru.tinkoff.ru.seminar.model;

import java.util.List;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface WeatherApi {
    @GET("data/2.5/weather?APPID=8f6c2573ac388537d70cbc4d4e24b325")
    Single<Weather> getWeather(@Query("q") String location);

    @GET("data/2.5/forecast?APPID=8f6c2573ac388537d70cbc4d4e24b325")
    Single<WeatherList> getForecast(@Query("q") String location);
}
