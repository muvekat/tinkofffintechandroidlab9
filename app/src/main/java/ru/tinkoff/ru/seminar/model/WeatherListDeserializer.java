package ru.tinkoff.ru.seminar.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class WeatherListDeserializer implements JsonDeserializer<WeatherList> {
    @Override
    public WeatherList deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();

        JsonArray weatherJsonArray = jsonObject.getAsJsonArray("list");
        List<Weather> weatherList = new ArrayList<>();
        Weather tmpWeather;
        for (JsonElement elem: weatherJsonArray){
            tmpWeather = context.deserialize(elem, Weather.class);
            weatherList.add(tmpWeather);
        }

        return new WeatherList(weatherList);
    }
}
